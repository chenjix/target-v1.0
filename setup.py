from setuptools import setup, find_packages

setup(name='target',
      version='1.0',
      description='',
      long_description='',
      install_requires=['pandas', 'numpy', 'matplotlib', 'scipy', 'tqdm'],
      keywords='',
      packages=find_packages(),
      author='',
      author_email='',
      license='',
      include_package_data=True,
      package_data={'': ['example/data/*']})
